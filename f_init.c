#include "fossil.h"
#include <dos.h>
#include <stdlib.h>

int F_Init(void)
{
        union REGS      regs;
        regs.h.al = 0;
        regs.h.ah = 0x04;       /* function = initialize driver */
        regs.x.dx = (unsigned char) port;
        regs.x.bx = 0;          /* no ^C */
        int86(FOSSIL_INT, &regs, &regs);
        if (regs.x.ax != 0x1954)
                return (FALSE); /* unable to initialize fossil */
        else if (regs.h.bl < 0x18 || regs.h.bh < 5) {
                return (FALSE);
        } else
                return (TRUE);
}
