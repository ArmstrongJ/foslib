#include "fossil.h"
#include <stdio.h>
#include <stdarg.h>

int cdecl F_Snd_Rw(char *fmt,...)
{
        int             val;
        va_list         marker;
        static char     buffer[318];
        va_start(marker, fmt);
        val = vsprintf(buffer, fmt, marker);
        va_end(marker);
        Send_Raw(buffer);
                return(TRUE);
}
