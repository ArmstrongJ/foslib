FOSSIL Library 1.05b by Ken Wetz
Modifications by Jeff Armstrong <jeff@rainbow-100.com>

This library, originally authored by Ken Wetz, provides a simple C 
interface to a FOSSIL (Fido-Opus-Seadog Standard Interface Layer) 
driver, normally used for serial communications on MS-DOS and OS/2.  The 
original source code was marked as "Freeware," and we'll assume, without 
any additional information or restrictions explicitly set, that the code 
is public domain at this time.

This modification of the library includes minor changes to allow 
compiling with Microsoft C 5 or higher and Turbo C.  It also provides an 
additional function, F_NBPeek(), for performing a peek on the input 
stream in a non-blocking manner.  Finally, this version allows the 
application to set a flag to ignore carrier detection, useful if you're 
speaking with a peer that does not provide such signals.

The included Makefile works delightfully well with Turbo C's make 
utility.  It also suprisingly will work with Microsoft's lobotomized 
make utility shipped with MSC 5.1.

Please feel free to copy and share this library.

Jeff Armstrong
October 5, 2015

