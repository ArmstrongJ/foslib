#include "fossil.h"
#include <dos.h>
#include <stdlib.h>

int F_Set_Baud(long baud)
{
        union REGS      regs;
        int  value;

        switch (baud) {
        case 300:
                value = 0x43;  /* sets also 8 bit words, No parity, 1 stopbit */
                break;
        case 600:
                value = 0x63;
                break;
        case 1200:
                value = 0x83;
                break;
        case 2400:
                value = 0xA3;
                break;
        case 4800:
                value = 0xC3;
                break;
        case 9600:
                value = 0xE3;
                break;
        case 19200:
                value = 3;
                break;
        case 38400:
                value = 0x23;
                break;
        default:
                return (FALSE);
        }

        regs.h.ah = 0x00;
        regs.h.al = (unsigned char) value;
        regs.x.dx = (unsigned char) port;
        int86(FOSSIL_INT, &regs, &regs);
        return (regs.x.ax);
}
