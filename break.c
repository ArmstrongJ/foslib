#include "fossil.h"
#include <dos.h>
#include <stdlib.h>

void Break(unsigned char function)
{
        union REGS      regs;

        regs.h.ah = 0x1A;
        regs.h.al = function;
        regs.x.dx = (unsigned char) port;
        int86(FOSSIL_INT, &regs, &regs);
        return;
}
