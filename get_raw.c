#include "fossil.h"
#include <dos.h>
#include <stdlib.h>

unsigned char Get_Raw(void)
{
        union REGS      regs;

                        /* Changed by jba -> regs.h.ah = 0x20; */
                        regs.h.ah = 0x02;
                        regs.h.al = 0x00;
                        regs.x.dx = (unsigned char) port;
                        int86(FOSSIL_INT, &regs, &regs);
                        regs.h.ah = 0x00;
                        if(regs.x.ax == 0xFFFF) return(FALSE);
                        return(unsigned char)regs.h.al;
               }
