#include <stdlib.h>
#include <dos.h>
#include "fossil.h"
unsigned char  Key_Read(void)
{

        union REGS      regs;
        int c;
        regs.h.ah = 0x0E;
        regs.h.al = 0x00;
        int86(FOSSIL_INT, &regs, &regs);
        c = regs.h.al & 0xff;
        return(unsigned char)c;
}
