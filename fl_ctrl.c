#include "fossil.h"
#include <dos.h>
#include <stdlib.h>

void Fl_Ctrl(unsigned char function)
{
        union REGS      regs;

        regs.h.ah = 0x0F;
        regs.h.al = function;
        regs.x.dx = (unsigned char) port;
        int86(FOSSIL_INT, &regs, &regs);
        return;
}
