#include "fossil.h"
#include  <time.h>

int              Timer(double timelimit)
{
clock_t time_now;
time_now = clock();
if(timelimit < time_now)
{
return(FALSE);
}
return(TRUE);
}
