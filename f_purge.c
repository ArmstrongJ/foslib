#include "fossil.h"
#include <dos.h>

void F_Purge_Out()
{
        union REGS      regs;

        regs.h.ah = 0x09;     /* function = request status */
        regs.h.al = 0x00;
        regs.x.dx = (unsigned char) port;
        int86(FOSSIL_INT, &regs, &regs);
        return;
 }

