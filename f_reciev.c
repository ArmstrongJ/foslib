#include "fossil.h"
#include <dos.h>
#include <conio.h>
#include <stdlib.h>

unsigned char F_Receive_Wait()
{
        union REGS      regs;
        unsigned char   key;
        double time_allowed;
        time_allowed = Set_Time(30);           /* sets the timer function */
        do {                    /* Watch carrier while waiting for next char */
                if (!local) {
                        if (F_Get_Status(CK_DCD) == FALSE)
                                return(FALSE);
               }
                if (Timer(time_allowed) == FALSE) /* if no one types it times */
                        return(FALSE);            /* out after 30 seconds   */
                if (Kb_Hit()) { /* Allows local keyboard over-ride */
                        key = (unsigned char) getche();
                        if (key == 0 || key == 0xe0)
                                key = (unsigned char) getche();
                        return(key);
               }}
                        while (!F_Peek());
                        if(!local) {
                        regs.h.ah = 0x02;
                        regs.h.al = 0x00;
                        regs.x.dx = port;
                        int86(FOSSIL_INT, &regs, &regs);
                        regs.h.ah = 0x00;
                        return(unsigned char)regs.h.al; }
                        return(FALSE);
               }
