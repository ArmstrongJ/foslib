#include "fossil.h"
#include <stdio.h>
#include <stdarg.h>

int cdecl F_Printf( char *fmt,...)
{
        int             val;
        va_list         marker;
        static char     buffer[512];
        va_start(marker, fmt);
        val = vsprintf(buffer, fmt, marker);
        va_end(marker);
        if (!F_Puts(buffer))
        {
                return (FALSE);
        }
        return(val);
}
