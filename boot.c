#include "fossil.h"
#include <dos.h>
#include <stdlib.h>

void Boot(unsigned char function)
{
        union REGS      regs;

        regs.h.ah = 0x17;
        regs.h.al = (unsigned char) function;
        regs.x.dx = (unsigned char) port;
        int86(FOSSIL_INT, &regs, &regs);
        return;
}
