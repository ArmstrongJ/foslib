#include "fossil.h"
#include <dos.h>

void F_De_Init(void)
{
        union REGS      regs;

        regs.h.ah = 0x05;       /* function = deactivate port */
        regs.h.al = 0x00;
        regs.x.dx = (unsigned char) port;
        int86(FOSSIL_INT, &regs, &regs);
        return;
}
