#include "fossil.h"
#include <dos.h>

void F_Purge_In()
{
        union REGS      regs;

        regs.h.ah = 0x0A;       /* Purge input buffer */
        regs.h.al = 0x00;
        regs.x.dx = (unsigned char) port;
        int86(FOSSIL_INT, &regs, &regs);
        return;
 }
