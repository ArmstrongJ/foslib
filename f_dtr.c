#include "fossil.h"
#include <dos.h>
#include <stdlib.h>

void F_Dtr(unsigned char chr)
{
        union REGS      regs;

        regs.h.ah = 0x06;       /* function = request status */
        regs.h.al = chr;
        regs.x.dx = (unsigned char) port;
        int86(FOSSIL_INT, &regs, &regs);
        return;
 }
