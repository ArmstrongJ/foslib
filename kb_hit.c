#include <stdlib.h>
#include <dos.h>
#include "fossil.h"

int              Kb_Hit(void)
{
        union REGS      regs;
        regs.h.ah = 0x0d;
        regs.h.al = 0x00;
        int86(FOSSIL_INT, &regs, &regs);
        if(regs.x.ax == 0xffff)
        return(FALSE);
        return(TRUE);
}
