#include "fossil.h"
#include <dos.h>
#include <stdlib.h>

unsigned char F_Peek(void)
{
        union REGS      regs;
        regs.h.ah = 0x0C;
        regs.x.dx = (unsigned char) port;
        int86(FOSSIL_INT, &regs, &regs);
        if(regs.x.ax == 0xFFFF)
        return(FALSE);
        return (regs.h.al);
}
