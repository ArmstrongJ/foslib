#include "fossil.h"
#include <dos.h>

int F_Send_Char(unsigned char function, unsigned char arg)
{
        union REGS      regs;

        regs.h.al = arg;
        regs.h.ah = function;
        regs.x.dx = (unsigned char) port;
        int86(FOSSIL_INT, &regs, &regs);
        return(regs.x.ax);
}
