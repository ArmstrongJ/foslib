 #include <stdio.h>
#include <time.h>
#include "fossil.h"
void Pause(long seconds)
{
clock_t done_now;
done_now = clock() + seconds * CLOCKS_PER_SEC;
while( clock() < done_now )
{;}
}
