#include "fossil.h"
#include <dos.h>
#include <stdlib.h>
int F_Get_Status(unsigned char function)
{
        int         arg = 0;

        union REGS      regs;

        if(function == CK_DCD && dcd_ignore)
                return TRUE;

        regs.h.ah = 0x03;       /* function = request status */
        regs.h.al = 0x00;
        regs.x.dx = (unsigned char) port;
        int86(FOSSIL_INT, &regs, &regs);


        switch (function) {
        case 1:
                arg = ((regs.h.al & 0x10) == 0x10) ? 1 : 0;  /* CTS * /
                        break;
        case 2:
                arg = ((regs.h.al & 0x20) == 0x20) ? 1 : 0;  /* DSR */
                break;
        case 3:
                arg = ((regs.h.al & 0x40) == 0x40) ? 1 : 0;  /* RING */
                break;
        case 4:
                arg = ((regs.h.al & 0x80) == 0x80) ? 1 : 0;   /* DCD */
                break;
        case 5:
                arg = ((regs.h.ah & 0x00) == 0x00) ? 1 : 0;   /* RDA */
                break;
        case 6:
                arg = ((regs.h.ah & 0x02) == 0x02) ? 1 : 0;   /* OVRN */
                break;
        case 7:
                arg = ((regs.h.ah & 0x20) == 0x20) ? 1 : 0;   /* THRE */
                break;
        case 8:
                arg = ((regs.h.ah & 0x40) == 0x040) ? 1 : 0;  /* TSRE */
                break;
        case 9:
                arg = ((regs.h.ah & 0x80) == 0x80) ? 1 : 0;   /* TIMEOUT */
        default:
                return (FALSE);
        }
        return (arg);
}
