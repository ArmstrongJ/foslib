extern unsigned char port; /* 0 comm 1 1 comm 2 */
extern unsigned char vidio_mode;
extern int     local;      /* if set to true routines will not send out comm port */
extern int     dcd_ignore; /* check for carrier detect before doing work */
#define FOSSIL_INT   0x14
#define TRANSMIT     0x0b  /* Send now no wait */
#define TRANSWAIT    0x01  /* Send with wait, dangerous if RXONXOFF is enabled */
#define RXONXOFF     0x01  /* allows remote to use xon xoff flow control */
#define CTSRTS       0x02  /* allows cts rts flow control */
#define TXONXOFF     0x08  /* allows local to use xon xoff flow control */
#define BOTHFLOW     0x0D  /* allows both XON XOFF and CTS RTS flow contol */
#define NOFLOW       0x00  /* Turns off all flow control */
#define NO_CK        0x02  /* turns off Ctrl CK checking */
#define YES_CK       0x03  /* Turns on Ctrl CK Cttl_Ck() = TRUE if recieved */
#define TX_OFF       0x00  /* Stops the  transmiter */
#define TX_ON        0x02  /* Restarts the transmitter  */
#define DOG_ON       0x01  /* enables watchdog reboots system if no carrier */
#define DOG_OFF      0x00  /* turns watchdog back off */
#define COLDBOOT     0x00  /* instant cold boot of system ! */
#define WARMBOOT     0x01  /* instant warm boot of system ! */
#define TRUE         1
#define FALSE        0
#define ANSI         0x13  /* allows local screen to see ansi */
#define DOS          0x15  /* dos mode, no ansi will show */
#define BREAK        0x01  /* start sending break */
#define BREAKOFF     0x00  /* finished with break */
#define CK_CTS       1     /* Used in F_Get_Status, see's if CTS is high */
#define CK_DSR       2     /* dito  checks if DSR is high */
#define CK_RING      3     /* dito checks if comm reports a ring */
#define CK_DCD       4     /* dito checks if carrier is present */
#define CK_RDA       5     /* dito checks if data availible in input buffer */
#define CK_OVRN      6     /* dito if true the input buffer has been overrun */
#define CK_THRE      7     /* dito checks if room is available in output buffer */
#define CK_TSRE      8     /* dito if true the output buffer is empty */
#define CK_TIMEOUT   9     /* dito checks if Fossil timed out  */
typedef unsigned bit;        /* beats typing out what these really are !! */
typedef unsigned int word;
typedef unsigned char byte;
