#include "fossil.h"
#include <dos.h>
#include <stdlib.h>

int Ctrl_Ck(unsigned char function)
{
        union REGS      regs;

        regs.h.ah = 0x10;
        regs.h.al = function;
        regs.x.dx = (unsigned char) port;
        int86(FOSSIL_INT, &regs, &regs);
        return(regs.x.ax);
}
